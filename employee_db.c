/*
* Authors: Julia Cassella and Leah Sato
* September 20, 2017
* COMP280 Project #1 
* This project simulates an employee databse
* The inital db contents are read in from a file which is a command line argument
* Then the user can manipulate the the db by adding new eployees
* updating employees
* ordering the db in a specified way
* or searching for employee by last name or id number 
*/

#include <stdio.h>      // the C standard I/O library
#include <stdlib.h>     // the C standard library
#include <string.h>     // the C string library
#include "readfile.h"   // my file reading routines
#include <ctype.h>

// Definitions
#define MAXFILENAME  128
#define MAXNAME       64
#define MAXDBSIZE	1024
#define TRUE 1
#define FALSE 0

// Defining Employee
struct Employee {
    char last[MAXNAME];
    char first[MAXNAME];
    int salary;
    int id;
};

 // The following line allows us to use "Employee" rather than 
// "struct Employee" throughout this code.
typedef struct Employee Employee;

// forward declaration of functions
void getFilenameFromCommandLine(char filename[], int argc, char *argv[]);
void readAndAddFile(char *filename, struct Employee *db, int *db_size_ptr);
void print(struct Employee *db, int *db_size_ptr);
void add(struct Employee emp, struct Employee *db, int *db_size_ptr);
void getID(int ID, struct Employee *db, int start, int end);
void getLast(char name[], struct Employee *db,int *db_size_ptr);
int newEmp(struct Employee *db, int *db_size_ptr);
void sort_by_id(struct Employee *db,int *db_size_ptr, int *sorted_by_id);
int compare_id(const void *e1, const void *e2);
void update(struct Employee *db, int *db_size_ptr);
void sort_by_last(struct Employee *db, int *db_size_ptr, int *sorted_by_id);
int compare_last(const void *e1, const void *e2);
char* toUpperCase(char str[]);

/**
 * This is the main method. 
 */
int main (int argc, char *argv[]) {
	char filename[MAXFILENAME];

	//local variable for the size of the db 
	int db_size;

	//pointer to db_size
	int *db_size_ptr = &db_size;
	int sorted_by_id = TRUE;

	// this initializes the filename string from the command line arguments
	getFilenameFromCommandLine(filename, argc, argv);

	// Creates an array or Employees
	struct Employee *db = calloc(MAXDBSIZE, sizeof(struct Employee));
	readAndAddFile(filename, db, db_size_ptr);
	sort_by_id(db, db_size_ptr, &sorted_by_id);
	
	// Continually ask user for option and break when quit.
	int option;
	while (TRUE) {
	    printf("Employee DB Menu: \n ------------------------ \n (1) Print the Database.\n (2) Lookup by ID. \n (3) Lookup by Last Name. \n (4) Add an Employee. \n (5) Quit\n (6) Update Employee Info \n (7) Sort Employees by last name. \n (8) Sort Employees by ID\n ------------------------\nEnter your choice: ");
   		scanf("%d", &option);
		
   		// print databate
   		if (option == 1) {
			print(db, db_size_ptr);
   		}
   		// look up by id
   		else if (option == 2) {
			
			//make sure that the db is currently sorted by id 
			if(sorted_by_id == FALSE) {
				
				//sort by id (currently sorted by name)
				sort_by_id(db, db_size_ptr, &sorted_by_id);
				//call our method
				printf("Enter your id number: ");
   				int ID;
   				scanf("%d", &ID);
				getID(ID, db, 0, *db_size_ptr);
				//resort by last name
				sort_by_last(db,db_size_ptr, &sorted_by_id);
			}
			//we are currently sorted by id (just have to call the method)
			else {
   				printf("Enter your id number: ");
   				int ID;
   				scanf("%d", &ID);
				
				int start = 0;
   				getID(ID, db, start, db_size);
			}
   		}
   		// look up by last name
   		else if (option == 3) {
   			printf("Enter your Last Name: ");
   			char in[MAXNAME];
   			scanf("%s", in);
   			getLast(in, db, db_size_ptr);
   		}
   		// add employee
   		else if (option == 4) {
   			newEmp(db, db_size_ptr);
			if(sorted_by_id == FALSE) {
				sort_by_last(db,db_size_ptr, &sorted_by_id);
			}
			else {
				sort_by_id(db, db_size_ptr, &sorted_by_id);
			}
   		}
   		// quit
   		else if (option == 5) {
   			break;
   		}
   		// update employee info
   		else if (option == 6) {
			update(db, db_size_ptr);
			if(sorted_by_id == FALSE) {
				printf("resorting after update");
				sort_by_last(db,db_size_ptr, &sorted_by_id);
			}
			else {
				sort_by_id(db, db_size_ptr, &sorted_by_id);
			}
   		}
   		// sort employees by last name
   		else if (option == 7) {
   		
		   sort_by_last(db,db_size_ptr, &sorted_by_id);
			
   		}
   		// sort employees by id
   		else if (option == 8) {
			sort_by_id(db, db_size_ptr, &sorted_by_id);
   		}
   		else {
   			printf("Invalid option. Valid options are 1-8.\n");
   		}
	}	

   printf("\ngood bye\n");
   return 0;
}


 /*
* This is a function will add a new employee into the db
* @param emp the new employee we are adding 
* @param db our current db of employees
* @param db_size_ptr the current size of the db
*/
void add(struct Employee emp, struct Employee *db, int *db_size_ptr) {
	db[*db_size_ptr -1] = emp;
}

/**
 * Method that will update the employee information
 * @param db our current db of employees
 * @oaram db_size_ptr the current size of the db
 */
 
void update(struct Employee *db, int *db_size_ptr) {
  //get the id from the user 
  printf("Enter Employee ID Number: ");  
  int emp_id;
  scanf("%d", &emp_id);
  getID(emp_id, db, 0, *db_size_ptr);
  //search for that employee 
  for (int i = 0; i < *db_size_ptr; i++) {
		if(db[i].id == emp_id) {
			//allow the user to update the information
			printf("Select what info you would like to change For the employee above (enter number)\n 1. First Name \n 2. Last Name \n 3. Salary \n 4. id \n 5. Done With Update \n ");
			//Until the user selects quit option they will be propted to update which fieds they wish
			while(TRUE) {
				printf("Enter your choice: ");
			    int choice; 
				scanf("%d", &choice);
			    //update First Name
				if(choice == 1) {
			        char new_first[MAXNAME];
			        printf("Enter in new first name: ");
			        scanf("%s", new_first);
			        strcpy(db[i].first, new_first);
			    }
				//Update Last name 
			    else if(choice == 2 ){
			        char new_last[MAXNAME];
			        printf("Enter in new last name: ");
			        scanf("%s", new_last);
			        strcpy(db[i].last , new_last);
			    }
				//update salary 
			    else if(choice == 3){
			        int new_sal;
			        printf("Enter in new salary: ");
			        scanf("%d", &new_sal);
					if(new_sal < 30000 || new_sal > 150000) {
						printf("Invalid Salary Entered. Salary must be between 30000 and 1500000");
					}
			        db[i].salary = new_sal;
			    }
				//update id
			    else if(choice == 4 ){
			        int new_id;
			        printf("Enter in new ID Number: ");
			        scanf("%d", &new_id);
					if (new_id < 100000 || new_id > 999999) {
						printf("Invalid ID Entered. ID must be between 100000 and 999999");
					}
					else {
						db[i].id = new_id;
					}
			        
			    }
				//exit the program update option
			    else if(choice == 5 ){
			        break;

			    }
				//They entered an invalid option 
				else {
					printf("Invalid option: The only valid options are 1 - 5 \n");
				}
			    
			}
			break;
		}
	}

    
}

/**
 * This is allow us to sort the program by id
 * @param db our current database of employees
 * @param db_size_ptr the size of our database
 * @param sorted_by_id that will indicate how the db is currently sored 1 = sorted by id 0 = sorted by last name
 */
void sort_by_id(struct Employee *db, int *db_size_ptr, int *sorted_by_id) {

    qsort(db, *db_size_ptr , sizeof(struct Employee), compare_id);
	*sorted_by_id = TRUE;
}

/**
* This method allows the user to sort by last name
 * @param db our current database of employees
 * @param db_size_ptr the size of our database
 * @param sorted_by_id that will indicate how the db is currently sored 1 = sorted by id 0 = sorted by last name
* 
*/
void sort_by_last(struct Employee *db, int *db_size_ptr, int *sorted_by_id) {

	qsort(db, *db_size_ptr , sizeof(struct Employee), compare_last);
	*sorted_by_id = FALSE;
}

/**
* Compare method that is used to compare the ids of two employees to determine which is greater
* @param e1 const void pointer (this will be an employee)
* @param e2 const void pointer (this will be an employee)
*/
int compare_id(const void *in1,const void *in2)
{
	const struct Employee *e1 = in1;
	const struct Employee *e2 = in2;
    
    int com_result;
    if(e1->id < e2->id)
        com_result = -1;
    else 
        com_result = 1;
        
    return com_result;
}

/**
* This will compare the last name of two employees to determine which one is greater, if they are 
* The same then we will compare the first names of the employees. 
* @param e1 const void pointer (this will be an employee)
* @param e2 const void pointer (this will be an employee)
*/
int compare_last(const void *in1, const void *in2)
{
    const struct Employee *e1 = in1;
	const struct Employee *e2 = in2;
    int com_result;
	//create a copy of the first last name that we can use to compare in all caps
	char temp1[MAXNAME];
	strcpy(temp1, e1->last);
	toUpperCase(temp1);
	//create a copy of the second last name that we can use to compare in all caps
	char temp2[MAXNAME];
	strcpy(temp2,e2->last);
	toUpperCase(temp2);
	
    com_result = strcmp(temp1, temp2);
	//if they have the same last name compare the first name instead
	if(com_result ==0) {
			//create a copy of the first first name that we can use to compare in all caps
		char temp3[MAXNAME];
		strcpy(temp3, e1->first);
		toUpperCase(temp3);
		
		//create a copy of the second first name that we can use to compare in all caps
		char temp4[MAXNAME];
		strcpy(temp4, e2->first);
		toUpperCase(temp4);
		com_result = strcmp(temp3, temp4);
	}
	return com_result;
}


/**
 * Method that will convert a string into upper case
 * @param str will be the string we are going to convert to all caps 
 * @return: a string that is in all caps
 */
char* toUpperCase(char str[])
{ 
      int i = 0; 

      while(str[i] != '\0') 
      { 
        if(str[i] >= 'a' && str[i] <= 'z') 
        	str[i] = str[i] + ('A' - 'a'); 
        i++; 
      }
      return str;
}

/**
 * Function to print out the database
 * @param db our current database of employees
 * @param db_size_ptr the size of our database
 */
void print(struct Employee *db, int *db_size_ptr) {
    printf("NAME\t\t\t\t\tSALARY\tID\n");
    printf("-----------------------------------------------------------\n");
    int i;
	for (i = 1; i < *db_size_ptr; i++) {
        printf("%-12s\t%-12s\t\t%d\t%d\n", db[i].first, db[i].last, db[i].salary, db[i].id);
	}
	printf("Number of employees: %d\n", (i-1));
}

/**
* This method will search to find the input id using binary search method. 
* The inputs will be the ID we are searching for, the database, the start index, and the end index
* of the range we are searching in. 
* This method will be recursivly called. 
* @param ID the id number that we are searching for
* @param db the database of employees
* @param start the start index of our search 
* @param end the end index of our search 
*/

void getID(int ID, struct Employee *db, int start, int end){
	
	
	//base case is if the id = the "middle of the start and the end" or if we have searched everything (start = end)
	int middle = start + (end - start) /2;
	
	if( ID == db[middle].id) {
		//then we found our id and print off the string 
		printf("Last name: %s, First name: %s, Salary: %d\n", db[middle].last, db[middle].first, db[middle].salary);
		return;
		
	}
	//second base case is if we have checked the entire db and we did not find the id number
	else if(start == middle || middle == end) {
		printf("No such ID\n");
		return;
	}
	//else if the id is larger than the id in the middle so we check the top half
	else if(ID > db[middle].id) {
		getID(ID, db, middle, end);
	}
	//else the id must be less than the middle value so we check the bottom half
	else {
		getID(ID, db, start, middle);
	}
	
	
}

/** 
 * Function to find an employee by last name
 * @param name the name that we are searching for
 * @param db our current database of employees
 * @param db_size_ptr the size of our database
 */
void getLast(char name[], struct Employee *db, int *db_size_ptr) {
	int found = 0;
	//loop through all the db until we find the employee with the matching last name
	for (int i = 0; i < *db_size_ptr; i++) {
		if(strcmp(db[i].last,name) == 0) {
			printf("First name: %s, Salary: %d, ID: %d \n", db[i].first, db[i].salary, db[i].id);
			found = 1;
			break;
		}
	}

	if(found == 0) {
		printf("No such last name\n");
	}
	
}

/**
 * This will allow the user to enter in all the fields for the new employee they wish to add
 * They will be prompted to add each individual field 
 * @param db our current database of employees
 * @param db_size_ptr the size of our database
 */
int newEmp(struct Employee *db, int *db_size_ptr) {
	
	struct Employee emp;
	//get the first name (must start with capital letter)
	printf("Please enter first name: ");
	scanf("%s", emp.first);
	//get the last name (must start with a capital letter)
	printf("Please enter last name: ");
	scanf("%s", emp.last);
	//prompt user to enter in a salary until a valid salary is entered
	while (TRUE) {
		printf("Please enter salary: ");
		int temp_sal;
		scanf("%d", &temp_sal);
		if (temp_sal < 30000 || temp_sal > 150000){
			printf("Invalid Salary: Salary must be between 30000 and 150000 \n");
		}
		else {
			emp.salary = temp_sal;
			break;
		}
	}
	//prompt user to enter an id number until a valid id number is entered	
	while (TRUE) {
		printf("Please enter id: ");
		int temp_id;
		scanf("%d", &temp_id);
		if (temp_id < 100000 || temp_id > 999999) {
			printf("Invalid ID: ID must be between 100000 and 999999 \n");
		}
		else {
			emp.id = temp_id;
			break;
		}
	}
	//print out the new employee and ask if the user wants to add it 
	while(TRUE) {
		printf("First: %s\nLast: %s\nSalary: %d\nID: %d\n", emp.first, emp.last, emp.salary, emp.id);
		printf("Do you want to add employee?\n1. yes\n0. no\n");
		int answer;
		scanf("%d", &answer);
		if (answer == 0) {
			return FALSE;
		}
		else if(answer == 1) {
			*db_size_ptr +=1;
			add(emp, db, db_size_ptr);
			
			return TRUE;
		}
		else {
			printf("Invalid option. Please Select 1 or 0. \n");
		}
	}
	
	
}

/**
 * Read through the file and add all the employees into the db
 * @param filename the file that we are pulling the employees out of
 * @param db our current database of employees
 * @param db_size_ptr the size of our database
 */
void readAndAddFile(char *filename, struct Employee *db, int *db_size_ptr) {

   int ret = openFile(filename);  // try to open the DB file
   if (ret == -1) {
       printf("bad error: can't open %s\n", filename);
        exit(1);
   }

   int id, salary;
   char fname[MAXNAME], lname[MAXNAME];
    while (ret != -1) {
		*db_size_ptr +=1; 
       ret = readInt(&id); 
	   if(id == 0) {break;}        // read the first line of values from file
       if (ret) { break; }
       ret = readString(fname);
       if (ret) { break; }
       ret = readString(lname);
       if (ret) { break; }
       ret = readInt(&salary);
       if (ret == 0) { // stuff was read in okay
       		struct Employee emp;
       		strcpy(emp.first, fname);
       		strcpy(emp.last, lname);
       		emp.id = id;
       		emp.salary = salary;
       		add(emp, db, db_size_ptr);
       }
    }

    closeFile();  // when all done close the file
}

   
 /**
 *  DO NOT MODIFY THIS FUNCTION. It works "as is".
 *
 *  This function gets the filename passed in as a command line option
 *  and copies it into the filename parameter. It exits with an error 
 *  message if the command line is badly formed.
 *  @param filename the string to fill with the passed filename
 *  @param argc, argv the command line parameters from main 
 *               (number and strings array)
 */
void getFilenameFromCommandLine(char filename[], int argc, char *argv[]) {

    if (argc != 2) {
        printf("Usage: %s database_file\n", argv[0]);
        // exit function: quits the program immediately...some errors are not 
        // recoverable by the program, so exiting with an error message is 
        // reasonable error handling option in this case
        exit(1);
    }
    if (strlen(argv[1]) >= MAXFILENAME) {
        printf("Filename, %s, is too long, cp to shorter name and try again\n",
                filename);
        exit(1);
    }
    strcpy(filename, argv[1]);
}
